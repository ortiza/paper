# static class does not exist within python, but this is effectively one
from nltk.stem.wordnet import WordNetLemmatizer
import nltk.corpus
import pandas
from nltk.util import ngrams
from nltk import tokenize


# placeholder
def phrases(text, keyphrase):
    # get number of words in phrase
    n = len(keyphrase.split())
    # break text into ngrams
    ngram = ngrams(text, n)
    # return array
    return ngram


# returns tokens in their base form (going -> go)
def base_form(token):
    lemma = WordNetLemmatizer()
    base = lemma.lemmatize(token)
    return base


# prints matches (or no match found) for a single word in the text
def search_abstract_single_word(tokens, keyword):
    match = 0
    for token in tokens:
        if token == keyword:
            match += 1
    return match


def search_abstract_phrase(ngrams, phrase):
    matches = 0
    for item in ngrams:
        space = " "
        gram = space.join(item)
        if gram == phrase:
            matches += 1
    return matches


# searches the compiled list of abstracts for a single keyword
def search_file_single_word(abstract_list, keyword):
    matches = list()
    for item in abstract_list:
        tokens = item.get_tokens()
        match = search_abstract_single_word(tokens, keyword)
        matches.append(match)
    return matches


def search_file_phrase(abstract_list, keyphrase):
    matches = list()
    for item in abstract_list:
        ngrams = phrases(item.get_tokens(), keyphrase)
        match = search_abstract_phrase(ngrams, keyphrase)
        matches.append(match)
    return matches


def search_file_combined(abstract_list, keyterms, lemmatize, mandatory):
    keys = keyterms.split(",")
    matches = list()

    #  for every item in keys, if there is a base form that is different, add it to the key list
    if lemmatize is True:
        count = 0
        for key in keys:
            item = base_form(key)
            if key is not item:
                keys.append(item)
            count += 1
    for item in keys:
        if len(item.split()) > 1:
            match = search_file_phrase(abstract_list, item)
        else:
            match = search_file_single_word(abstract_list, item)
        matches.append(match)

    rankings = rank(matches, abstract_list, mandatory)
    return rankings


# return list
def ranked_result_pages(abstract_list, ranking, keyterms):
    rank1 = list()
    rank2 = list()
    rank3 = list()
    rank_list = list()
    counter = 0
    terms = keyterms.split(",")
    # rankings =
    for item in ranking:
        if item >= len(terms):
            rank3.append(abstract_list[counter])
        elif item >= .5 * len(terms):
            rank2.append(abstract_list[counter])
        elif item >= 0:
            rank1.append(abstract_list[counter])
        counter = counter + 1

    rank_list.append(rank1)  # worst ranking at index 0
    rank_list.append(rank2)  # middle ranking at index 1
    rank_list.append(rank3)  # best ranking at index 2
    return rank_list  # list of lists


def excel_output(abstract_list):
    rows = []
    for item in abstract_list:
        data = {'Title': item.get_title(), 'Authors': item.get_authors(), 'Text': item.get_text()}
        rows.append(data)
    df = pandas.DataFrame(data=rows, columns=['Title', 'Author', 'Text'])
    writer = pandas.ExcelWriter('test.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Sheet1', index=False)
    writer.save()


def rank(match_list, abstract_list, mandatory):
    rankings = list()
    x = 0
    while x < len(abstract_list):
        count = 0
        terms_matched = 0
        total_matched = 0
        last = 0
        rank = 0
        length = len(match_list)  # to use to check mandatory terms

        for matches in match_list:
            if matches[x] > 0:
                terms_matched += 1
                total_matched += matches[x]

        # if "all keywords mandatory" box is checked, this throws out the abstracts that don't have all the terms in it
        if mandatory is True and length != terms_matched:
            rank = -1
            count += 1
            rankings.append(rank)
            # del abstract_list[x]
            # x -= 1  # cause there is now one less abstract in the abstract_list
            # terms_matched = 0
        else:
            rank += terms_matched
            if total_matched > 0:
                if len(abstract_list[count].get_tokens()) / total_matched < 5:
                    rank += 2
                elif len(abstract_list[count].get_tokens()) / total_matched < 7:
                    rank += 1
            count += 1
            rankings.append(rank)
        x += 1
    return rankings




