# import gui and interaction necessities
from tkinter import filedialog
from tkinter import *
from tkinter import simpledialog
from tkinter import messagebox
import tkinter.ttk as ttk
from paper_abstract import PaperAbstract
from analytics import search_file_single_word
from analytics import search_file_combined
from analytics import excel_output
from analytics import ranked_result_pages
import pandas


class paperGUI:
    def __init__(self, master):
        self.master = master
        master.title("Systematic Review Software")

        self.filename = ""
        self.keywords = ""
        self.ranked_pages = []
        self.abstractDelete = ""
        self.relevancy =1 #test value for relevancy
        self.mandatory = False
        self.lemmatize = False
        '''This sets the colors for the program'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9'  # X11 color: 'gray85'
        _ana1color = '#d9d9d9'  # X11 color: 'gray85'
        _ana2color = '#d9d9d9'  # X11 color: 'gray85'

        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.', background=_bgcolor)
        self.style.configure('.', foreground=_fgcolor)
        self.style.configure('.', font="TkDefaultFont")
        self.style.map('.', background=
                       [('selected', _compcolor), ('active', _ana2color)])

        master.geometry("879x397+189+94")
        master.configure(borderwidth="2")
        master.configure(background="white")

        self.menubar = Menu(master, font="TkMenuFont", bg=_bgcolor, fg=_fgcolor)
        master.configure(menu=self.menubar)

        self.file = Menu(master, tearoff=0)
        self.menubar.add_cascade(menu=self.file,
                                 activebackground="#d9d9d9",
                                 activeforeground="#000000",
                                 background="#d9d9d9",
                                 font="TkMenuFont",
                                 foreground="#000000",
                                 label="File")
        self.file.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            font="TkMenuFont",
            foreground="#000000",
            label="Choose File",
            command=self.getFile)
        self.file.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            font="TkMenuFont",
            foreground="#000000",
            label="Export File",
            command=self.export)
        self.options = Menu(master, tearoff=0)
        self.menubar.add_cascade(menu=self.options,
                                 activebackground="lightblue",
                                 activeforeground="#000000",
                                 background="#d9d9d9",
                                 font="TkMenuFont",
                                 foreground="#000000",
                                 label="Options")
        self.options.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            font="TkMenuFont",
            foreground="#000000",
            label="Match Settings",
            command=self.both)
        self.options.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            font="TkMenuFont",
            foreground="#000000",
            label="Search Settings",
            command=self.mandatory_search)
        self.search = Menu(master, tearoff=0)
        self.menubar.add_cascade(menu=self.search,
                                 activebackground="#d9d9d9",
                                 activeforeground="#000000",
                                 background="#d9d9d9",
                                 compound="left",
                                 font="TkMenuFont",
                                 foreground="#000000",
                                 label="Search")
        self.search.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            compound="left",
            font="TkMenuFont",
            foreground="#000000",
            label="Save Search",
            command=self.save)
        self.search.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            compound="left",
            font="TkMenuFont",
            foreground="#000000",
            label="Run",
            command=self.run)
        self.search.add_command(
            activebackground="lightblue",
            activeforeground="#000000",
            background="#d9d9d9",
            compound="left",
            font="TkMenuFont",
            foreground="#000000",
            label="Clear All")

        self.SearchFrame = Frame(master)
        self.SearchFrame.place(relx=0.0, rely=0.12, relheight=0.1, relwidth=0.42)
        self.SearchFrame.configure(relief=GROOVE)
        self.SearchFrame.configure(borderwidth="2")
        self.SearchFrame.configure(relief=GROOVE)
        self.SearchFrame.configure(background="lightblue")
        self.SearchFrame.configure(highlightbackground="#d9d9d9")
        self.SearchFrame.configure(highlightcolor="black")
        self.SearchFrame.configure(width=375)

        self.keywords_box = Text(self.SearchFrame)
        self.keywords_box.place(relx=0.03, rely=0.29,height=20, relwidth=0.94)
        self.keywords_box.configure(background="white")
        self.keywords_box.configure(borderwidth="2")
        self.keywords_box.configure(foreground="#000000")
        self.keywords_box.configure(highlightbackground="#d9d9d9")
        self.keywords_box.configure(highlightcolor="black")
        self.keywords_box.configure(insertbackground="black")
        self.keywords_box.configure(relief=GROOVE)
        self.keywords_box.configure(selectbackground="#c4c4c4")
        self.keywords_box.configure(selectforeground="black")

        self.command = Text(master)
        self.command.place(relx=0.01, rely=0.23, relheight=0.76, relwidth=0.41)
        self.command.configure(background="white")
        self.command.configure(borderwidth="2")
        self.command.configure(foreground="#000000")
        self.command.configure(highlightbackground="#d9d9d9")
        self.command.configure(highlightcolor="black")
        self.command.configure(relief=GROOVE)
        self.command.configure(selectbackground="#c4c4c4")
        self.command.configure(selectforeground="black")
        self.command.configure(width=364)

        self.Save = ttk.Button(master)
        self.Save.place(relx=0.22, rely=0.0, height=35, width=86)
        self.Save.configure(takefocus="")
        self.Save.configure(text='''Save Keyword''')
        self.Save.configure(command=self.save)

        self.RunButton = ttk.Button(master)
        self.RunButton.place(relx=0.32, rely=0.0, height=35, width=86)
        self.RunButton.configure(takefocus="")
        self.RunButton.configure(text='''Run''')
        self.RunButton.configure(command=self.run)

        self.Clear = ttk.Button(master)
        self.Clear.place(relx=0.42, rely=0.0, height=35, width=86)
        self.Clear.configure(takefocus="")
        self.Clear.configure(text='''Clear All''')
        self.Clear.configure(command=self.clear)

        self.SearchLabel = ttk.Label(master)
        self.SearchLabel.place(relx=0.01, rely=0.03, height=19, width=176)
        self.SearchLabel.configure(foreground="#000000")
        self.SearchLabel.configure(font="TkDefaultFont")
        self.SearchLabel.configure(relief=FLAT)
        self.SearchLabel.configure(text='''Enter a keyword to search with''')
        self.SearchLabel.configure(width=176)
        self.SearchLabel.configure(background="lightblue")

        self.images = (

            PhotoImage("img_close", data='''R0lGODlhDAAMAIQUADIyMjc3Nzk5OT09PT
                         8/P0JCQkVFRU1NTU5OTlFRUVZWVmBgYGF hYWlpaXt7e6CgoLm5ucLCwszMzNbW
                         1v//////////////////////////////////// ///////////yH5BAEKAB8ALA
                         AAAAAMAAwAAAUt4CeOZGmaA5mSyQCIwhCUSwEIxHHW+ fkxBgPiBDwshCWHQfc5
                         KkoNUtRHpYYAADs= '''),

            PhotoImage("img_closeactive", data='''R0lGODlhDAAMAIQcALwuEtIzFL46
                         INY0Fdk2FsQ8IdhAI9pAIttCJNlKLtpLL9pMMMNTP cVTPdpZQOBbQd60rN+1rf
                         Czp+zLxPbMxPLX0vHY0/fY0/rm4vvx8Pvy8fzy8P//////// ///////yH5BAEK
                         AB8ALAAAAAAMAAwAAAVHYLQQZEkukWKuxEgg1EPCcilx24NcHGYWFhx P0zANBE
                         GOhhFYGSocTsax2imDOdNtiez9JszjpEg4EAaA5jlNUEASLFICEgIAOw== '''),

            PhotoImage("img_closepressed", data='''R0lGODlhDAAMAIQeAJ8nD64qELE
                         rELMsEqIyG6cyG7U1HLY2HrY3HrhBKrlCK6pGM7lD LKtHM7pKNL5MNtiViNaon
                         +GqoNSyq9WzrNyyqtuzq+O0que/t+bIwubJw+vJw+vTz+zT z////////yH5BAE
                         KAB8ALAAAAAAMAAwAAAVJIMUMZEkylGKuwzgc0kPCcgl123NcHWYW Fs6Gp2mYB
                         IRgR7MIrAwVDifjWO2WwZzpxkxyfKVCpImMGAeIgQDgVLMHikmCRUpMQgA7 ''')
        )

        self.style.element_create("close", "image", "img_close",
                                  ("active", "pressed", "!disabled", "img_closepressed"),
                                  ("active", "alternate", "!disabled",
                                   "img_closeactive"), border=8, sticky='')

        self.style.layout("ClosetabNotebook", [("ClosetabNotebook.client",
                                                {"sticky": "nswe"})])
        self.style.layout("ClosetabNotebook.Tab", [
            ("ClosetabNotebook.tab",
             {"sticky": "nswe",
              "children": [
                  ("ClosetabNotebook.padding", {
                      "side": "top",
                      "sticky": "nswe",
                      "children": [
                          ("ClosetabNotebook.focus", {
                              "side": "top",
                              "sticky": "nswe",
                              "children": [
                                  ("ClosetabNotebook.label", {"side":
                                                                  "left", "sticky": ''}),
                                  ("ClosetabNotebook.close", {"side":
                                                                  "left", "sticky": ''}), ]})]})]})])

        PNOTEBOOK = "ClosetabNotebook"

        self.style.configure('TNotebook.Tab', background=_bgcolor)
        self.style.configure('TNotebook.Tab', foreground=_fgcolor)
        self.style.map('TNotebook.Tab', background=
        [('selected', _compcolor), ('active', _ana2color)])
        self.PNotebook1 = ttk.Notebook(master)
        self.PNotebook1.place(relx=0.43, rely=0.18, relheight=0.77
                              , relwidth=0.55)

        self.PNotebook1.configure(width=300)
        self.PNotebook1.configure(takefocus="")
        self.PNotebook1.configure(style=PNOTEBOOK)
        self.PNotebook1_t0 = Frame(self.PNotebook1)
        self.PNotebook1.add(self.PNotebook1_t0, padding=3)
        self.PNotebook1.tab(0, text="Likely", compound="none", underline="-1", )
        self.PNotebook1_t0.configure(relief=GROOVE)
        self.PNotebook1_t0.configure(background="lightblue")
        self.PNotebook1_t0.configure(highlightbackground="#d9d9d9")
        self.PNotebook1_t0.configure(highlightcolor="black")

        self.PNotebook1_t1 = Frame(self.PNotebook1)
        self.PNotebook1.add(self.PNotebook1_t1, padding=3)
        self.PNotebook1.tab(1, text="Possible", compound="none", underline="-1", )
        self.PNotebook1_t1.configure(background="lightblue")
        self.PNotebook1_t1.configure(highlightbackground="#d9d9d9")
        self.PNotebook1_t1.configure(highlightcolor="black")

        self.PNotebook1_t2 = Frame(self.PNotebook1)
        self.PNotebook1.add(self.PNotebook1_t2, padding=3)
        self.PNotebook1.tab(2, text="Unlikely", compound="none", underline="-1", )
        self.PNotebook1_t2.configure(relief=GROOVE)
        self.PNotebook1_t2.configure(background="lightblue")
        self.PNotebook1_t2.configure(highlightbackground="#d9d9d9")
        self.PNotebook1_t2.configure(highlightcolor="black")

        self.scrollbar1 = Scrollbar(self.PNotebook1_t0)

        self.Text1 = Text(self.PNotebook1_t0, yscrollcommand=self.scrollbar1.set)
        self.Text1.place(relx=0.02, rely=0.03, height=290, relwidth=0.92)
        self.Text1.configure(background="white")
        self.Text1.configure(font="TkFixedFont")
        self.Text1.configure(foreground="#000000")
        self.Text1.configure(insertbackground="black")
        self.Text1.configure(width=464)
        self.Text1.configure(cursor="hand2")

        self.scrollbar1.pack(side=RIGHT, fill=Y)
        self.scrollbar1.config(command=self.Text1.yview)

        self.scrollbar2 = Scrollbar(self.PNotebook1_t1)

        self.Text2 = Text(self.PNotebook1_t1, yscrollcommand=self.scrollbar2.set)
        self.Text2.place(relx=0.02, rely=0.03, height=290, relwidth=0.92)
        self.Text2.configure(background="white")
        self.Text2.configure(font="TkFixedFont")
        self.Text2.configure(foreground="#000000")
        self.Text2.configure(insertbackground="black")
        self.Text2.configure(width=464)
        self.Text2.configure(cursor="hand2")

        self.scrollbar2.pack(side=RIGHT, fill=Y)
        self.scrollbar2.config(command=self.Text2.yview)

        self.scrollbar3 = Scrollbar(self.PNotebook1_t2)

        self.Text3 = Text(self.PNotebook1_t2, yscrollcommand=self.scrollbar3.set)
        self.Text3.place(relx=0.02, rely=0.03, height=290, relwidth=0.92)
        self.Text3.configure(background="white")
        self.Text3.configure(font="TkFixedFont")
        self.Text3.configure(foreground="#000000")
        self.Text3.configure(insertbackground="black")
        self.Text3.configure(width=464)
        self.Text3.configure(cursor="hand2")

        self.scrollbar3.pack(side=RIGHT, fill=Y)
        self.scrollbar3.config(command=self.Text3.yview)

        self.pageEntry = Text(master)
        self.pageEntry.place(relx=0.56, rely=0.1, height=20, relwidth=0.3)
        self.pageEntry.configure(background="white")
        self.pageEntry.configure(font="TkFixedFont")
        self.pageEntry.configure(foreground="#000000")
        self.pageEntry.configure(insertbackground="black")
        self.pageEntry.configure(width=264)

        self.SearchLabel2 = Label(master)
        self.SearchLabel2.place(relx=0.56, rely=0.03, height=21, width=114)
        self.SearchLabel2.configure(activebackground="#f9f9f9")
        self.SearchLabel2.configure(activeforeground="black")
        self.SearchLabel2.configure(background="light blue")
        self.SearchLabel2.configure(disabledforeground="#a3a3a3")
        self.SearchLabel2.configure(foreground="#000000")
        self.SearchLabel2.configure(highlightbackground="#d9d9d9")
        self.SearchLabel2.configure(highlightcolor="black")
        self.SearchLabel2.configure(text='''(page #, Abstract #)''')

        self.deletePage = ttk.Button(master)
        self.deletePage.place(relx=0.88, rely=0.08, height=35, width=92)
        self.deletePage.configure(takefocus="")
        self.deletePage.configure(text='''Delete Abstract''')
        self.deletePage.configure(width=86)
        self.deletePage.configure(command=self.deleteAbstract)

        self.Text1.tag_configure("hidden", elide=True)
        self.Text1.tag_configure("header", background="black", foreground="white", spacing1=10, spacing3=10)
        self.Text1.tag_bind("header", "<Double-1>", self._toggle_visibility)

        self.Text2.tag_configure("hidden", elide=True)
        self.Text2.tag_configure("header", background="black", foreground="white", spacing1=10, spacing3=10)
        self.Text2.tag_bind("header", "<Double-1>", self._toggle_visibility2)

        self.Text3.tag_configure("hidden", elide=True)
        self.Text3.tag_configure("header", background="black", foreground="white", spacing1=10, spacing3=10)
        self.Text3.tag_bind("header", "<Double-1>", self._toggle_visibility3)

        self.master.mainloop()

    def write(self, s):
        self.command.insert(END, s)

    def getFile(self):
        self.filename = filedialog.askopenfilename(initialdir="/", title="Select file",
                                                   filetypes=(("Excel files", "*.xlsx"), ("all files", "*.*")))

    def clear(self):
        self.filename = ""
        self.keywords = ""
        self.save()

    def save(self):
        self.keywords = self.keywords_box.get("1.0", 'end - 1c')
        self.keywords_box.delete("1.0", END)
        self.command.delete("1.0", END)
        self.command.insert("1.0", "Search file: " + self.filename + "\nfor keywords:" + self.keywords)

    def run(self):
        search_input = self.keywords
        file_input = self.filename

        # initiate object holder and file reader
        abstract_list = list()
        abstract_file = pandas.read_excel(file_input, header=None)

        # create paper abstract objects
        for row in abstract_file.itertuples():
            abstract = PaperAbstract(row[1], row[2], row[3])
            abstract_list.append(abstract)


        # search abstracts for keyword
        matches = search_file_combined(abstract_list, search_input, self.lemmatize, self.mandatory)
        self.ranked_pages = ranked_result_pages(abstract_list, matches, search_input)

        if self.relevancy is 1:
            for item in self.ranked_pages[2]:
                self.Text1.insert(END, "Abstract %s\n" % self.ranked_pages[2].index(item), "header")
                self.Text1.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
            for item in self.ranked_pages[1]:
                self.Text2.insert(END, "Abstract %s\n" % self.ranked_pages[1].index(item), "header")
                self.Text2.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
            for item in self.ranked_pages[0]:
                self.Text3.insert(END, "Abstract %s\n" % self.ranked_pages[0].index(item), "header")
                self.Text3.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
        elif self.relevancy is 2:
            for item in self.ranked_pages[2]:
                self.Text1.insert(END, "Abstract %s\n" % self.ranked_pages[2].index(item), "header")
                self.Text1.insert(END, "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() +
                                  "\n\nText: " + item.get_text() + "\n\n\n")
            for item in self.ranked_pages[1]:
                self.Text2.insert(END, "Abstract %s\n" % self.ranked_pages[1].index(item), "header")
                self.Text2.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
        else:
            for item in self.ranked_pages[2]:
                self.Text1.insert(END, "Abstract %s\n" % self.ranked_pages[2].index(item), "header")
                self.Text1.insert(END, "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() +
                                  "\n\nText: " + item.get_text() + "\n\n\n")

    def deleteAbstract(self):

        self.abstractDelete = self.pageEntry.get("1.0", END)
        self.pageEntry.delete("1.0", END)
        deleteContents = self.abstractDelete.split(",")
        page = int(deleteContents[0])
        abstractNum = int(deleteContents[1])
        abstract = ''
        if page == 1:
            abstract = self.ranked_pages[2].__getitem__(abstractNum)
            self.ranked_pages[2].remove(abstract)

        elif page == 2:
            abstract = self.ranked_pages[1].__getitem__(abstractNum)
            self.ranked_pages[1].remove(abstract)
        else:
            abstract = self.ranked_pages[0].__getitem__(abstractNum)
            self.ranked_pages[0].remove(abstract)

        self.Text1.delete("1.0", END)
        self.Text2.delete("1.0", END)
        self.Text3.delete("1.0", END)

        if self.relevancy is 1:
            for item in self.ranked_pages[2]:
                self.Text1.insert(END, "Abstract %s\n" % self.ranked_pages[2].index(item), "header")
                self.Text1.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
            for item in self.ranked_pages[1]:
                self.Text2.insert(END, "Abstract %s\n" % self.ranked_pages[1].index(item), "header")
                self.Text2.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
            for item in self.ranked_pages[0]:
                self.Text3.insert(END, "Abstract %s\n" % self.ranked_pages[0].index(item), "header")
                self.Text3.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
        elif self.relevancy is 2:
            for item in self.ranked_pages[2]:
                self.Text1.insert(END, "Abstract %s\n" % self.ranked_pages[2].index(item), "header")
                self.Text1.insert(END, "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() +
                                  "\n\nText: " + item.get_text() + "\n\n\n")
            for item in self.ranked_pages[1]:
                self.Text2.insert(END, "Abstract %s\n" % self.ranked_pages[1].index(item), "header")
                self.Text2.insert(END,
                                  "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() + "\n\nText: "
                                  + item.get_text() + "\n\n\n")
        else:
            for item in self.ranked_pages[2]:
                self.Text1.insert(END, "Abstract %s\n" % self.ranked_pages[2].index(item), "header")
                self.Text1.insert(END, "Authors: " + item.get_authors() + "\n\nTitle: " + item.get_title() +
                                  "\n\nText: " + item.get_text() + "\n\n\n")

    def _toggle_visibility(self, event):

        block_start, block_end = self._get_block("insert")
        # is any of the text tagged with "hidden"? If so, show it
        next_hidden = self.Text1.tag_nextrange("hidden", block_start, block_end)
        if next_hidden:
            self.Text1.tag_remove("hidden", block_start, block_end)
        else:
            self.Text1.tag_add("hidden", block_start, block_end)

    def _toggle_visibility2(self, event):

        block_start, block_end = self._get_block2("insert")
        # is any of the text tagged with "hidden"? If so, show it
        next_hidden = self.Text2.tag_nextrange("hidden", block_start, block_end)
        if next_hidden:
            self.Text2.tag_remove("hidden", block_start, block_end)
        else:
            self.Text2.tag_add("hidden", block_start, block_end)

    def _toggle_visibility3(self, event):

        block_start, block_end = self._get_block3("insert")
        # is any of the text tagged with "hidden"? If so, show it
        next_hidden = self.Text3.tag_nextrange("hidden", block_start, block_end)
        if next_hidden:
            self.Text3.tag_remove("hidden", block_start, block_end)
        else:
            self.Text3.tag_add("hidden", block_start, block_end)

    def _get_block(self, index):

        start = self.Text1.index("%s lineend+1c" % index)
        next_header = self.Text1.tag_nextrange("header", start)
        if next_header:
            end = next_header[0]
        else:
            end = self.Text1.index("end-1c")
        return start, end

    def _get_block2(self, index):

        start = self.Text2.index("%s lineend+1c" % index)
        next_header = self.Text2.tag_nextrange("header", start)
        if next_header:
            end = next_header[0]
        else:
            end = self.Text2.index("end-1c")
        return start, end

    def _get_block3(self, index):

        start = self.Text3.index("%s lineend+1c" % index)
        next_header = self.Text3.tag_nextrange("header", start)
        if next_header:
            end = next_header[0]
        else:
            end = self.Text3.index("end-1c")
        return start, end

    def mandatory_search(self):
        answer = messagebox.askyesno("Question", "Do you want to enforce all keywords as mandatory?")
        if answer is True:
            self.mandatory = True

    def lemma_match(self):
        answer = messagebox.askyesno("Question", "Do you want to lemmatize searches (takes longer but more matches)?")
        if answer is True:
            self.lemmatize = True

    def min_match(self):
        answer = simpledialog.askstring("Input", "What minimum relevancy do you want to enforce")
        if answer is not None:
            if int(answer) > 1:
                self.relevancy = answer

    def both(self):
        self.lemma_match()
        self.min_match()

    def export(self):
        temp = self.ranked_pages[0] + self.ranked_pages[1]
        temp2 = temp + self.ranked_pages[2]
        excel_output(temp2)
