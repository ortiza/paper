import unittest
from paper_abstract import *
from analytics import *
from nltk import tokenize


class Testing(unittest.TestCase):
    def test_get_author(self):
        paper = PaperAbstract("Foo", "Bar", "USMC")
        self.assertEqual(paper.get_authors(), "Bar")

    def test_get_text(self):
        paper = PaperAbstract("Foo", "Bar", "USMC")
        self.assertEqual(paper.get_text(), "USMC")

    def test_get_title(self):
        paper = PaperAbstract("Foo", "Bar", "USMC")
        self.assertEqual(paper.get_title(), "Foo")

    def test_phrases(self):
        phrase = "This is a test"
        keyword = "for science"
        tokens = tokenize.word_tokenize(phrase)
        test = phrases(tokens, keyword)
        test = list(test)
        self.assertEqual(test[0][1], 'is')
        self.assertEqual(test[1][0], 'is')

    def test_base_form(self):
        self.assertEqual(base_form('feet'), 'foot')
        self.assertEqual(base_form('car'), 'car')

    def test_parts_of_speech(self):
        pass

    def test_search_abstract_single_word(self):
        tokens = ['these', 'are', 'tokens', 'i', 'guess']
        keyword = "guess"
        self.assertEqual(search_abstract_single_word(tokens, keyword), 1)
        keyword = "lol"
        self.assertEqual(search_abstract_single_word(tokens, keyword), 0)

    def test_search_abstract_phrase(self):
        tokens = [('these', 'are'), ('are', 'tokens')]
        keyword = "are tokens"
        self.assertEqual(search_abstract_phrase(tokens, keyword), 1)
        keyword = "lol hi"
        self.assertEqual(search_abstract_phrase(tokens, keyword), 0)

    def test_search_file_single_word(self):
        paper1 = PaperAbstract("Foo", "Bar", "text")
        paper2 = PaperAbstract("Perp", "berp", "computer")
        paper_list = (paper1, paper2)
        keyword_a = "text"
        self.assertEqual(search_file_single_word(paper_list, keyword_a), [1, 0])
        keyword_b = "computer"
        self.assertEqual(search_file_single_word(paper_list, keyword_b), [0, 1])
        keyword_c = "lol"
        self.assertEqual(search_file_single_word(paper_list, keyword_c), [0, 0])

    def test_search_file_phrase(self):
        paper1 = PaperAbstract("Foo", "Bar", "this is an abstract about bees wax")
        paper2 = PaperAbstract("Perp", "berp", "this is about a red pony")
        paper_list = (paper1, paper2)
        keyword_a = "bees wax"
        self.assertEqual(search_file_phrase(paper_list, keyword_a), [1, 0])
        keyword_b = "red pony"
        self.assertEqual(search_file_phrase(paper_list, keyword_b), [0, 1])
        keyword_c = "lol wot"
        self.assertEqual(search_file_phrase(paper_list, keyword_c), [0, 0])

    def test_search_file_combined(self):
        testPaper1 = PaperAbstract("Title", "Author", "Computer")
        testPaper2 = PaperAbstract("Title 2", "Author 2", "Computer Science")
        testList = (testPaper1, testPaper2)
        keywords = "Computer,Computer Science"
        matches = search_file_combined(testList, keywords)
        self.assertEqual(matches[0], 1)
        self.assertEqual(matches[1], 2)

    def test_excel_output(self):
        pass


if __name__ == '__main__':
    unittest.main()
