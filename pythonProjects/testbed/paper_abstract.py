''' Author: Heather Willson willsonh@etsu.edu
    Date:   6/21/2018
    Purpose: this class creates an instance of a paper abstract
    i have used python naming conventions ... it's weird looking, sorry :D '''

from nltk import tokenize

class PaperAbstract:
    # constructor with 3 arguments. 2 underscores in front of the field names means they're private
    def __init__(self, title, authors, text):
        self.__title = title
        self.__authors = authors
        self.__text = text
        self.__tokens = self.tokenizer()

    def get_title(self):
        return self.__title

    def get_authors(self):
        return self.__authors

    def get_text(self):
        return self.__text

    def tokenizer(self):
        tokens = tokenize.word_tokenize(self.get_text())
        return tokens

    def get_tokens(self):
        return self.__tokens
